# Changelog

## 0.1.0
- Added `SingleInstance` template class (single instance pattern) 
- Added `Subject` template class (subject/observer pattern)
- Added `Hysteresis` and `SignalFilter` class
- Added `stringhelper` functions converting _char array_ to `std::string`
