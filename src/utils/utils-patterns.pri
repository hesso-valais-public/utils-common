
SRC_PATH = $$PWD

INCLUDEPATH += \
    $${SRC_PATH} \
    $${SRC_PATH}/..

# Input
HEADERS += \
    "$${SRC_PATH}/single/instance.h" \
    "$${SRC_PATH}/observer/subject.h" \
    "$${SRC_PATH}/observer/subjectwithcallback.h" \
    "$${SRC_PATH}/block/hysteresis.h" \
    "$${SRC_PATH}/block/interface/hysteresisobserver.h" \
    "$${SRC_PATH}/block/signalfilter.h"

#SOURCES += \

SOURCES += \
    "$${SRC_PATH}/block/hysteresis.cpp" \
    "$${SRC_PATH}/block/interface/hysteresisobserver.cpp" \
    "$${SRC_PATH}/block/signalfilter.cpp"
