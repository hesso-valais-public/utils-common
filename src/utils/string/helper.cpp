#include "helper.h"

namespace stringhelper
{

std::string toString(const char * const format, ...)
{
    va_list args;
    va_start(args, format);
    std::string str = toString(format, args);
    va_end(args);

    return str;
}

std::string toString(const char * const format, va_list args)
{
    va_list copy;
    va_copy(copy, args);
    const int len = std::vsnprintf(nullptr, 0, format, copy);

    if (len >= 0)
    {
        std::string str(std::size_t(len) + 1, '\0');
        std::vsnprintf(&str[0], str.size(), format, args);
        return str;
    }

    return std::string();
}

} // namespace stringhelper
