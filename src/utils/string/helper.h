#ifndef STRING_HELPER_H
#define STRING_HELPER_H

#include <string>
#include <cstdarg>

namespace stringhelper
{

std::string toString(const char * const format, ...);               ///< Converts a char string into a std::string using variadic arguments.
std::string toString(const char * const format, va_list args);      ///< Converts a char string into a std::string using va_list arguments.

} // namespace stringhelper

#endif // STRING_HELPER_H
