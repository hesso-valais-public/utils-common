#ifndef UTILS_OBSERVER_SUBJECT_WITH_CALLBACK_H
#define UTILS_OBSERVER_SUBJECT_WITH_CALLBACK_H

#include <stdint.h>
#include <cassert>
#include <functional>
#include <list>

/**
 * @brief SubjectWithCallback class to implement the subject/observer pattern.
 *
 * The CallbackSignature parameter should be the function signature
 * of the callback method to be called.
 */
template<typename CallbackSignature> class SubjectWithCallback
{
public:
    using CallbackHandler = std::function<CallbackSignature>;

    SubjectWithCallback() = default;

    bool subscribe(CallbackHandler callbackHandler, bool notifyActState = true)
    {
        observer.push_back(callbackHandler);

        if (notifyActState)
        {
            notifyActualState(callbackHandler);
        }

        return true;
    }

    virtual void notifyActualState(CallbackHandler callbackHandler) = 0;

    bool unsubscribe(CallbackHandler callbackHandler)
    {
        for (typename std::list<CallbackHandler>::const_iterator it = observer.begin();
             it != observer.end();
             it++)
        {
#if defined(__GXX_RTTI)
            // There is no '==' operator for std::function
            // See https://stackoverflow.com/questions/20833453/comparing-stdfunctions-for-equality
            // Compare function address stored in std::function
            if ((*it).target_type() == callbackHandler.target_type() and
                (*it).template target<CallbackSignature* >() == callbackHandler.template target<CallbackSignature* >())
            {
                observer.erase(it);
                return true;
            }
#else
            assert(false);  // Works only with RTTI enabled (remove -fno-rtti compiler flag)
            // See: https://stackoverflow.com/q/26907586
#endif
        }

        return false;   // Observer to remove not found
    }

    inline uint8_t observerCount() const { return observer.size(); }

protected:
    std::list<CallbackHandler> observer;        ///< List of callback handlers.
};

#endif // UTILS_OBSERVER_SUBJECT_WITH_CALLBACK_H
