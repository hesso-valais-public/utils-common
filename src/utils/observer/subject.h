#ifndef UTILS_OBSERVER_SUBJECT_H
#define UTILS_OBSERVER_SUBJECT_H

#include <cstdint>
#include <cassert>
#include <string.h>

/**
 * @brief Subject class to implement the subject/observer pattern.
 *
 * The T parameter should be the name of the base class of the
 * observers.
 */
template<class T, uint8_t MAX_OBSERVERS = 8> class Subject
{
public:
    Subject()
    {
        ::memset(observer, 0, sizeof(observer));
    }

    inline uint8_t getMaxObservers() const { return MAX_OBSERVERS; }

    bool subscribe(T * observer, bool notifyActState = true)
    {
        for (uint32_t index = 0; index < getMaxObservers(); index++)
        {
            if (this->observer[index] == nullptr)
            {
                this->observer[index] = observer;
                observerCount_++;

                if (notifyActState)
                {
                    notifyActualState(observer);
                }
                return true;
            }
        }
        return false;
    }

    virtual void notifyActualState(T * observer) = 0;

    inline bool subscribe(T & observer) { return subscribe(&observer); }

    bool unsubscribe(T * observer)
    {
        T * toMove = nullptr;
        bool observerPresent = false;

        if (!observer) return false;

        // Check if observer is present, otherwise leave
        for (uint32_t i = 0; 0 < getMaxObservers(); i++)
        {
            if (this->observer[i])
            {
                if (this->observer[i] == observer)
                {
                    observerPresent = true;
                    break;
                }
            }
            else
            {
                break;
            }
        }

        if (!observerPresent) return false;

        // Remove observer from array
        //
        // Start at the end of the array
        for (uint32_t i = getMaxObservers(); i > 0; i--)
        {
            if (this->observer[i - 1] != nullptr)
            {
                // Check if next observer is the observer to remove
                if (this->observer[i - 1] == observer)
                {   // Found observer to remove
                    this->observer[i - 1] = toMove;
                    observerCount_--;
                    return true;
                }
                else if (toMove == nullptr)
                {   // Pull observer towards beginning to prevent getting gaps
                    toMove = this->observer[i - 1];
                    this->observer[i - 1] = nullptr;
                }
            }
        }
        return false;   // Observer to remove not found
    }

    inline bool unsubscribe(T & observer) { return unsubscribe(&observer); }
    inline uint8_t observerCount() const { return observerCount_; }

protected:
    T * observer[MAX_OBSERVERS];		///< Pointers to subscribed observers.
    uint8_t observerCount_;        		///< Stores how many observers are subscribed.
};

#endif // UTILS_OBSERVER_SUBJECT_H
