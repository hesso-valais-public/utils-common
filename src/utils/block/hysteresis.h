#ifndef BLOCK_HYSTERESIS_H
#define BLOCK_HYSTERESIS_H

#include "utils/observer/subjectwithcallback.h"

namespace block {

// Forward declaration
template<class TIn, class TOut, typename CallbackSignature>
class Hysteresis;

typedef enum { LOW, HIGH } HysteresisOutput;
using HysteresisCallback = void(const block::HysteresisOutput & ouput);

/**
 * @brief Provides hysteresis on input value with callback functionality when passing limits.
 */
template<class TIn = float, class TOut = float, typename CallbackSignature = HysteresisCallback>
class Hysteresis : public SubjectWithCallback<CallbackSignature>
{
    using Base = SubjectWithCallback<CallbackSignature>;
public:
    Hysteresis(const TIn & lowThreshold, const TIn & highThreshold,
               const TOut & initialOutputValue);
    Hysteresis(const TIn & value = static_cast<TIn>(0), const TIn & threshold = static_cast<TIn>(0));

    void initialize(const TIn & lowThreshold, const TIn & highThreshold, const TOut & initialOutputValue);
    void initialize(const TIn & value, const TIn & threshold);
    void setInitialOutputValue(const TOut & initialOutputValue);

    TIn operator =(const TIn & in);
    operator TOut() const;

    inline bool isHigh() const { return (toHysteresisOutput(output_) == HysteresisOutput::HIGH);  }
    inline bool isLow() const { return (toHysteresisOutput(output_) == HysteresisOutput::LOW);  }

    // Subject interface implementation
protected:
    void notifyActualState(typename Base::CallbackHandler callbackHandler) override;

protected:
    void notify(const HysteresisOutput & output);
    inline HysteresisOutput toHysteresisOutput(const TOut & output) const { return (output == highValueThreshold_) ? HysteresisOutput::HIGH : HysteresisOutput::LOW; }

private:
    TIn lowValueThreshold_;         ///< Low value threshold.
    TIn highValueThreshold_;        ///< High value threshold.

    TOut output_;                   ///< Actual output value.
};

template<class TIn, class TOut, typename CallbackSignature>
Hysteresis<TIn, TOut, CallbackSignature>::Hysteresis(const TIn & lowThreshold, const TIn & highThreshold,
                                  const TOut & initialOutputValue)
{
    initialize(lowThreshold, highThreshold, initialOutputValue);
}

template<class TIn, class TOut, typename CallbackSignature>
Hysteresis<TIn, TOut, CallbackSignature>::Hysteresis(const TIn & value /* = static_cast<TIn>(0) */,
                                  const TIn & threshold /* = static_cast<TIn>(0) */)
{
    initialize(value, threshold);
}

template<class TIn, class TOut, typename CallbackSignature>
void Hysteresis<TIn, TOut, CallbackSignature>::initialize(const TIn & lowThreshold, const TIn & highThreshold, const TOut & initialOutputValue)
{
    lowValueThreshold_ = lowThreshold;
    highValueThreshold_ = highThreshold;

    setInitialOutputValue(initialOutputValue);
}

template<class TIn, class TOut, typename CallbackSignature>
void Hysteresis<TIn, TOut, CallbackSignature>::initialize(const TIn & value, const TIn & threshold)
{
    initialize(value - threshold, value + threshold, value - threshold);
}

template<class TIn, class TOut, typename CallbackSignature>
void Hysteresis<TIn, TOut, CallbackSignature>::setInitialOutputValue(const TOut & initialOutputValue)
{
    assert(highValueThreshold_ != lowValueThreshold_);
    assert(highValueThreshold_ > lowValueThreshold_);

    output_ = (initialOutputValue >= highValueThreshold_) ? highValueThreshold_ : lowValueThreshold_;
}

template<class TIn, class TOut, typename CallbackSignature>
TIn Hysteresis<TIn, TOut, CallbackSignature>::operator =(const TIn & in)
{
    if (output_ >= highValueThreshold_)
    {
        if (in <= lowValueThreshold_)
        {
            output_ = lowValueThreshold_;
            notify(toHysteresisOutput(output_));
        }

    }
    else if (output_ <= lowValueThreshold_)
    {
        if (in >= highValueThreshold_)
        {
            output_ = highValueThreshold_;
            notify(toHysteresisOutput(output_));
        }
    }

    return output_;
}

template<class TIn, class TOut, typename CallbackSignature>
void Hysteresis<TIn, TOut, CallbackSignature>::notifyActualState(typename Base::CallbackHandler callbackHandler)
{
    callbackHandler(toHysteresisOutput(output_));
}

template<class TIn, class TOut, typename CallbackSignature>
void Hysteresis<TIn, TOut, CallbackSignature>::notify(const HysteresisOutput & output)
{
    for (auto callbackHandler : Base::observer)
    {
        callbackHandler(output);
    }
}

template<class TIn, class TOut, typename CallbackSignature>
Hysteresis<TIn, TOut, CallbackSignature>::operator TOut() const
{
    return output_;
}

} // namespace block
#endif // BLOCK_HYSTERESIS_H
