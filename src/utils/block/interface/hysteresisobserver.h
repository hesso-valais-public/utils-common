#ifndef UTILS_BLOCK_INTERFACE_HYSTERESIS_OBSERVER_H
#define UTILS_BLOCK_INTERFACE_HYSTERESIS_OBSERVER_H

#include <stdint.h>

namespace block {
template<class TIn, class TOut, class Observer, uint8_t MAX_OBSERVERS>
class Hysteresis;
}

using HysteresisOutput = enum { LOW, HIGH };

namespace interface {

class NoneHysteresisObserver
{
public:
    void onOutputChangedTo(const HysteresisOutput & output)
    {
        (void)output;
    }
};

/**
 * @brief HysteresisObserver interface to get notified about changes.
 */
template<class Subject, class Observer> class HysteresisObserver
{
    friend Subject;         // To call callbacks defined below
public:
//    using HysteresisOutput = HysteresisOutput;

protected:
    virtual void onOutputChangedTo(const HysteresisOutput & output) = 0;

    static bool subscribeTo(Subject & hysteresis, Observer * observer);

    virtual ~HysteresisObserver() = default;

protected:
    HysteresisObserver() = default;

private:
    Subject * hysteresis_;                  ///< References to object observed.
};

//static
template<class Subject, class Observer>
bool HysteresisObserver<Subject, Observer>::subscribeTo(Subject & hysteresis,
                                                        Observer * observer)
{
    assert(observer);
    observer->hysteresis_ = &hysteresis;
    return observer->hysteresis_->subscribe(observer);
}

} // namespace interface
#endif // UTILS_BLOCK_INTERFACE_HYSTERESIS_OBSERVER_H
