#ifndef BLOCK_SIGNALFILTER_H
#define BLOCK_SIGNALFILTER_H


namespace block {

/**
 * @brief SignalFilter with bool input and bool output
 *
 * Used to filter glitches in received signals.
 */
class SignalFilter
{
public:
    SignalFilter(float gain = 0.8f,
                 float lowValueThreshold = 0.1f,
                 float highValueThreshold = 0.9f):
        gain(gain),
        lowValueThreshold(lowValueThreshold),
        highValueThreshold(highValueThreshold),
        oldValue(0),
        output(false)
    {
        gain = (gain > 1.0f) ? 1 : gain;
    }

    bool filter(bool actualValue)
    {
        float newValue = gain * actualValue + (1 - gain) * oldValue;

        oldValue = newValue;

        if (newValue >= highValueThreshold)
        {
            output = true;
        }
        else if (newValue <= lowValueThreshold)
        {
            output = false;
        }

        return output;
    }

    float filterOutput() const { return oldValue; }
    bool boolOutput() const { return output; }

protected:
    float gain;                 ///< Low-pass filter gain.
    float lowValueThreshold;    ///< Low value to switch to 'false'.
    float highValueThreshold;   ///< High value to switch to 'true'.

    float oldValue;             ///< Internal output value (float).
    bool output;                ///< Bool value returned by the filter.
};

} // namespace block

#endif // BLOCK_SIGNALFILTER_H
