#ifndef UTILS_SINGLE_INSTANCE_H
#define UTILS_SINGLE_INSTANCE_H

#include <cassert>

/**
 * @brief Adds 'Single Instance Pattern' functionality to a class.
 *
 * Derive from this class to be sure only one instance exists at a time.
 *
 * Give the name of the derived class as template parameter.
 */
template<class T> class SingleInstance
{
public:
    SingleInstance(T * _this)
    {
        assert(!singleton);
        singleton = _this;
    }

    static bool isInstancePresent() { return (singleton != nullptr); }
    static T & instance() { assert(singleton); return *singleton; }
    static T & getInstance() { assert(singleton); return *singleton; }

protected:
    static T * singleton;           ///< Pointer to single instance of this class.
};

/*
 * Define 'singleton' in the 'mysingleinstance.cpp' as follows:
 *
 * template <> MySingleInstance * SingleInstance<MySingleInstance>::singleton(nullptr);
 */

#define DEFINE_SINGLEINSTANCE_STATIC_SINGLETON_ATTRIBUTE(className) \
        template <> className * SingleInstance<className>::singleton(nullptr);

#endif // UTILS_SINGLE_INSTANCE_H
