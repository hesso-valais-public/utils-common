#include <cstdint>
#include <functional>
#include <catch2/catch_all.hpp>
#include "utils/block/hysteresis.h"

class TestObserver;

using HystersisInt = block::Hysteresis<int32_t, int32_t, void (const block::HysteresisOutput & output)>;

class TestObserver
{
public:
    using Subject = HystersisInt;

public:
    TestObserver() :
        callbackParameterOutput(block::HysteresisOutput::LOW),
        hysteresis_(nullptr),
        callbackCalled_(false)
    { }

    void onOutputChangedTo(const block::HysteresisOutput & output)
    {
        callbackCalled_ = true;
        callbackParameterOutput = output;
        INFO("onOutputChangedTo() called" << output);
    }

    void initialize(Subject & hysteresis)
    {
        hysteresis_ = &hysteresis;
        hysteresis.subscribe(std::bind(&TestObserver::onOutputChangedTo, this,
                                       std::placeholders::_1),
                             false);
    }

    void unsubscribe()
    {
        hysteresis_->unsubscribe(std::bind(&TestObserver::onOutputChangedTo, this,
                                           std::placeholders::_1));
    }

    bool callbackCalled() { bool c = callbackCalled_; callbackCalled_ = false; return c; }

    block::HysteresisOutput callbackParameterOutput;

protected:
    Subject * hysteresis_;
    bool callbackCalled_;
};

TEST_CASE("Hysteresis Subject/Observer callback LOW", "[callbacks]")
{
    const int32_t LOW_THRESHOLD = 0;
    const int32_t HIGH_THRESHOLD = 100;

    TestObserver observer;
    HystersisInt hystInt(LOW_THRESHOLD, HIGH_THRESHOLD, 110);

    observer.initialize(hystInt);

    std::vector<std::pair<int32_t, bool>> data{{ 105, false },
                                               {  90, false },
                                               {  60, false },
                                               {  50, false },
                                               {  30, false },
                                               {   0, true },
                                               { -30, false },
                                              };

    // Force to not expected value
    observer.callbackParameterOutput = block::HysteresisOutput::HIGH;

    for (auto value : data)
    {
        bool called;
        hystInt = value.first;                  // Set input

        called = observer.callbackCalled();
        REQUIRE(called == value.second);        // Check callback was called
        if (called)
        {
            REQUIRE(observer.callbackParameterOutput == block::HysteresisOutput::LOW);

            // Reset to not expected value
            observer.callbackParameterOutput = block::HysteresisOutput::HIGH;
        }
        else
        {
            CHECK(observer.callbackParameterOutput == block::HysteresisOutput::HIGH);
        }
    }

    observer.unsubscribe();
    CHECK(hystInt.observerCount() == 0);
}

TEST_CASE("Hysteresis Subject/Observer callback HIGH", "[callbacks]")
{
    const int32_t LOW_THRESHOLD = 0;
    const int32_t HIGH_THRESHOLD = 100;

    TestObserver observer;
    HystersisInt hystInt(LOW_THRESHOLD, HIGH_THRESHOLD, -32);

    observer.initialize(hystInt);

    std::vector<std::pair<int32_t, bool>> data{{ -30, false },
                                               {   0, false },
                                               {  30, false },
                                               {  50, false },
                                               {  60, false },
                                               {  90, false },
                                               { 100, true },
                                               { 105, false },
                                               { 110, false },
                                               {  99, false }
                                              };

    // Force to not expected value
    observer.callbackParameterOutput = block::HysteresisOutput::LOW;

    for (auto value : data)
    {
        bool called;
        hystInt = value.first;                  // Set input

        called = observer.callbackCalled();
        REQUIRE(called == value.second);        // Check callback was called
        if (called)
        {
            REQUIRE(observer.callbackParameterOutput == block::HysteresisOutput::HIGH);

            // Reset to not expected value
            observer.callbackParameterOutput = block::HysteresisOutput::LOW;
        }
        else
        {
            CHECK(observer.callbackParameterOutput == block::HysteresisOutput::LOW);
        }
    }
}
