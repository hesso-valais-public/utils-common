#include <catch2/catch_all.hpp>
#include "utils/block/hysteresis.h"

block::Hysteresis<float, float> hystFloat(5.0, 10.0, 11.0);

TEST_CASE("Hystersis instantiation", "[initalization]")
{
    hystFloat = 8.0;
    REQUIRE(hystFloat == 10.0);

    REQUIRE(block::Hysteresis<float, float>(0, 1, 0.5).operator float() == 0.0);
    REQUIRE(block::Hysteresis<float, int>(-5.0, 5.0, 100).operator int() == 5.0);
    REQUIRE(block::Hysteresis<int, bool>(-100, 100, true).operator bool() == true);
}

TEST_CASE("Signal crossing low threshold (high -> low transition)", "[whatever]")
{
    const float LOW_THRESHOLD = 5.0;
    const float HIGH_THRESHOLD = 10.0;

    std::vector<std::pair<float, float>> data{{ 11.0, HIGH_THRESHOLD },
                                              {  9.0, HIGH_THRESHOLD },
                                              {  6.0, HIGH_THRESHOLD },
                                              {  5.0, LOW_THRESHOLD },
                                              {  3.0, LOW_THRESHOLD },
                                              {  0.0, LOW_THRESHOLD },
                                              { -3.0, LOW_THRESHOLD },
                                             };

    block::Hysteresis<> smith(LOW_THRESHOLD, HIGH_THRESHOLD, 12.0);

    for (auto value : data)
    {
        smith = value.first;                // Set input
        REQUIRE(smith == value.second);     // output must match
    }
}

TEST_CASE("Signal crossing high threshold (low high transition)", "[whatever]")
{
    const float LOW_THRESHOLD = 5.0;
    const float HIGH_THRESHOLD = 10.0;

    std::vector<std::pair<float, float>> data{{ -3.0, LOW_THRESHOLD },
                                              {  0.0, LOW_THRESHOLD },
                                              {  3.0, LOW_THRESHOLD },
                                              {  5.0, LOW_THRESHOLD },
                                              {  6.0, LOW_THRESHOLD },
                                              {  9.0, LOW_THRESHOLD },
                                              { 10.0, HIGH_THRESHOLD },
                                              { 11.0, HIGH_THRESHOLD },
                                             };

    block::Hysteresis<> smith(LOW_THRESHOLD, HIGH_THRESHOLD, 8.0);

    for (auto value : data)
    {
        smith = value.first;                // Set input
        REQUIRE(smith == value.second);     // output must match
    }
}

TEST_CASE("Signal crossing low threshold (high -> low -> gray-zone -> low transition)", "[whatever]")
{
    const float LOW_THRESHOLD = 5.0;
    const float HIGH_THRESHOLD = 10.0;

    std::vector<std::pair<float, float>> data{{ 11.0, HIGH_THRESHOLD },
                                              {  9.0, HIGH_THRESHOLD },
                                              {  6.0, HIGH_THRESHOLD },
                                              {  5.0, LOW_THRESHOLD },
                                              {  3.0, LOW_THRESHOLD },
                                              {  0.0, LOW_THRESHOLD },
                                              {  2.0, LOW_THRESHOLD },
                                              {  5.0, LOW_THRESHOLD },
                                              {  7.0, LOW_THRESHOLD },
                                              {  6.0, LOW_THRESHOLD },
                                              {  5.0, LOW_THRESHOLD },
                                              {  2.0, LOW_THRESHOLD },
                                              {  0.0, LOW_THRESHOLD },
                                              { -3.0, LOW_THRESHOLD },
                                             };

    block::Hysteresis<> smith(LOW_THRESHOLD, HIGH_THRESHOLD, 11.0);

    for (auto value : data)
    {
        smith = value.first;                // Set input
        REQUIRE(smith == value.second);     // output must match
    }
}
