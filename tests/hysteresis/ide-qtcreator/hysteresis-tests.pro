TEMPLATE = app
QT += gui

#CONFIG += c++11
CONFIG += c++14

isEmpty(CATCH_INCLUDE_DIR): CATCH_INCLUDE_DIR=$$(CATCH_INCLUDE_DIR)
# set by Qt Creator wizard
isEmpty(CATCH_INCLUDE_DIR): CATCH_INCLUDE_DIR="C:/devel/Catch2-mingw810_32/include"
!isEmpty(CATCH_INCLUDE_DIR): INCLUDEPATH *= $${CATCH_INCLUDE_DIR}

LIBS += -L$${CATCH_INCLUDE_DIR}/../lib -lCatch2

isEmpty(CATCH_INCLUDE_DIR): {
    message("CATCH_INCLUDE_DIR is not set, assuming Catch2 can be found automatically in your system")
}

INCLUDEPATH += \
    ../../../src

SOURCES += \
    ../../../src/utils/block/hysteresis.cpp \
    main.cpp \
    tst_hysteresis.cpp \
    tst_hysteresis_callbacks.cpp

HEADERS += \
    ../../../src/utils/block/hysteresis.h
